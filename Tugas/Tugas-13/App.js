import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './components/Login';
import Register from './components/Register';
import About from './components/About';

export default class App extends Component {
  // alert(data.kind);

  render() {
    return (
      <View style={styles.container}>
        {/* <Login /> */}
        {/* <Register /> */}
        <About />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
