import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends Component {
  // alert(data.kind);
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navLogo}>
          <Image source={require('./../images/logo.png')} style={{ height: 100, }} />
        </View>
        <View style={styles.Title}>
          <Text style={{ fontSize: 25, color: "#003366" }}>Login</Text>
        </View>
        <View style={styles.TagInput}>
          <Text style={{ color: "#003366" }}>Username/Email</Text>
          <View style={styles.inputView}>
            <TextInput />
          </View>
          <Text style={{ color: "#003366" }}>Password</Text>
          <View style={styles.inputView}>
            <TextInput secureTextEntry={true} />
          </View>
        </View>
        <View style={styles.Tombol}>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Masuk</Text>
          </TouchableOpacity>
          <Text style={{ fontSize: 18, color: "#3EC6FF" }}>atau</Text>
          <TouchableOpacity style={styles.button2}>
            <Text style={styles.buttonText}>Daftar ?</Text>
          </TouchableOpacity>
        </View>
        <StatusBar style="auto" />

      </View>

    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },

  navLogo: {
    paddingTop: 90,
    alignItems: 'center'
  },
  Title: {
    alignItems: 'center',
    marginTop: 70
  },
  TagInput: {
    justifyContent: "center",
    margin: 30,
  },
  inputView: {
    width: "100%",
    borderColor: "#003366",
    borderWidth: 1,
    marginTop: 5,
    height: 50,
    marginBottom: 10,
    justifyContent: "center",
    paddingLeft: 5
  },
  Tombol: {
    alignItems: 'center'
  },
  button: {
    width: 200,
    backgroundColor: '#3EC6FF',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  button2: {
    width: 200,
    backgroundColor: '#003366',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center'
  }

});
