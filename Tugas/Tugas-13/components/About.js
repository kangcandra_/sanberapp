import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class About extends Component {


  render() {
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 25, color: "#003366" }}>Tentang Saya</Text>
        <View style={styles.circle}>
          <Icon name="person" size={50} style={{ textAlign: 'center', padding: 30 }}></Icon>
        </View>
        <Text style={{ fontSize: 20, color: "#003366" }}>Candra Herdiansyah</Text>
        <Text style={{ fontSize: 15, color: "#3EC6FF" }}>React Native Developer</Text>
        <View style={styles.Detail}>
          <Text>Portofolio</Text>
          <View style={{ borderBottomColor: '#003366', borderBottomWidth: 1, paddingTop: 5 }} />
          <View style={styles.ImageDetailRow}>
            <View style={styles.ImageDetailText}>
              <TouchableOpacity>
                <Icons name="gitlab" size={40} style={{ paddingLeft: 15 }}></Icons>
              </TouchableOpacity>
              <Text style={{ fontSize: 15, color: "#003366" }}>@kangcand</Text>
            </View>
            <View style={styles.ImageDetailText}>
              <TouchableOpacity>
                <Icons name="GitHub" size={40} style={{ marginLeft: 80 }}></Icons>
              </TouchableOpacity>
              <Text style={{ fontSize: 15, color: "#003366", marginLeft: 70 }}>@kangcand</Text>
            </View>
          </View>
        </View>
        <View style={styles.Detail2}>
          <Text>Hubungi Saya</Text>
          <View style={{ borderBottomColor: '#003366', borderBottomWidth: 1, paddingTop: 5 }} />
          <View style={styles.ImageDetailCoulumn}>
            <View style={styles.ImageDetailTextColumn}>
              <TouchableOpacity>
                <Icons name="facebook" size={40} style={{ marginLeft: 60 }}></Icons>
              </TouchableOpacity>
              <Text style={{ fontSize: 15, color: "#003366", paddingTop: 10, paddingLeft: 10 }}>@kangcandraa</Text>
            </View>
            <View style={styles.ImageDetailTextColumn}>
              <TouchableOpacity>
                <Icons name="instagram" size={40} style={{ marginLeft: 60 }}></Icons>
              </TouchableOpacity>
              <Text style={{ fontSize: 15, color: "#003366", paddingTop: 10, paddingLeft: 10 }}>@kangcandra_</Text>
            </View>
            <View style={styles.ImageDetailTextColumn}>
              <TouchableOpacity>
                <Icons name="twitter" size={40} style={{ marginLeft: 60 }}></Icons>
              </TouchableOpacity>
              <Text style={{ fontSize: 15, color: "#003366", paddingTop: 10, paddingLeft: 10 }}>@kangcandra_</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    // backgroundColor: '#fff',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  Detail: {
    backgroundColor: "#EFEFEF",
    padding: 5,
    width: 300,
    marginTop: 30
  },
  ImageDetailRow: {
    flexDirection: 'row',
    justifyContent: "center",
    padding: 10
  },
  ImageDetailText: {
    flexDirection: 'column'
  },
  Detail2: {
    backgroundColor: "#EFEFEF",
    width: 300,
    marginTop: 40,
    padding: 5
  },
  ImageDetailColumn: {
    flexDirection: 'column',
    justifyContent: "center",
    padding: 10
  },
  ImageDetailTextColumn: {
    flexDirection: 'row'
  },
  circle: {
    bottom: 10,
    left: 5,
    backgroundColor: '#EFEFEF',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#EFEFEF',
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30
  },


});
