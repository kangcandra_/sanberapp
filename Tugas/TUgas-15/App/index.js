import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator} from '@react-navigation/stack';

import { SignIn , CreateAccount} from './Screen'
import { StyleSheet, Text, View } from "react-native";


const AuthStack = createStacknavigator();

export default () => {
  <NavigationContainer>
    <AuthStack.Navigator>
      <AuthStack.Screen name="SignIn" componen={SignIn} />
      <AuthStack.Screen name="CreateAccount" componen={CreateAccount} />
    </AuthStack.Navigator>
  </NavigationContainer>
}
// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//     </View>
//   );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
