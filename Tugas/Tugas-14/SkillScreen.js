import React from 'react';
import { View, Text, ScrollView, Image, TextInput, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SkillItem from "./SkillItem";
import data from './skillData.json'

export default class SkillScreen extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={{ marginTop: 15, alignItems: 'flex-end' }}>
                    <Image source={require('./../../images/logo_sanber.png')} style={styles.logo} />
                </View>
                <View style={styles.kontak}>
                    <View style={styles.circle}>
                        <Icon name="person" size={20} />
                    </View>
                    <View style={styles.kontakName}>
                        <Text>Hai,</Text>
                        <Text>Candra Herdiansyah</Text>

                    </View>
                </View>
                <View style={styles.SkillTitle}>
                    <Text style={{ fontSize: 40, color: '#003366' }}>SKILL</Text>
                    <View style={{ borderBottomColor: '#B4E9FF', borderBottomWidth: 4, paddingTop: 5 }} />
                </View>
                <View style={styles.Kategori}>
                    <TouchableOpacity flex={4} style={styles.ButtonKategori}>
                        <Text>Library/Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity flex={4} style={styles.ButtonKategori}>
                        <Text>Bahasa Pemrograman</Text>
                    </TouchableOpacity>
                    <TouchableOpacity flex={2} style={styles.ButtonKategori}>
                        <Text>Teknologi</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={data.items}
                    renderItem={(skill) => <SkillItem skill={skill.item} />}
                    keyExtractor={(item) => item.id}
                />
            </ScrollView>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        width: 200,
        height: 60,

    },
    kontak: {
        marginTop: 30,
        marginLeft: 10,
        flexDirection: 'row'
    },
    circle: {
        // position: 'absolute',
        bottom: 10,
        top: 1,
        left: 5,
        backgroundColor: '#B4E9FF',
        borderRadius: 30,
        borderWidth: 1,
        borderColor: '#B4E9FF',
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
    },
    kontakName: {
        // backgroundColor: '#B4E9FF',
        marginLeft: 15
    },
    SkillTitle: {
        margin: 12,
        marginBottom: 0
    },
    Kategori: {
        flexDirection: 'row',
        padding: 10
    },

    ButtonKategori: {
        backgroundColor: '#B4E9FF',
        padding: 7,
        margin: 1,
        borderRadius: 20,
    },
    ListKategori: {
        backgroundColor: '#B4E9FF',
        flexDirection: 'row',
        margin: 10,
        marginTop: 0
    },
    LogoKategori: {
        padding: 5,
        color: '#003366'

    }


});
